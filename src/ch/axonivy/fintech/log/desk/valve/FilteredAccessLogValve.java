package ch.axonivy.fintech.log.desk.valve;

import java.io.IOException;
import javax.servlet.ServletException;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.AccessLogValve;
import ch.ivyteam.log.Logger;

public class FilteredAccessLogValve extends AccessLogValve {

	private static final Logger LOGGER = Logger.getLogger(FilteredAccessLogValve.class);
	@Override
	public void invoke(Request request, Response response) throws IOException, ServletException {
		LOGGER.error("############################# FilteredAccessLogValve");
		getNext().invoke(request, response);
	}
}
